using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace calculator
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new calculatorPage();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("OnStart was Called");
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("OnSleeo was Called");
        }

        protected override void OnResume()
        {
            Debug.WriteLine("OnResume was Called");
        }
    }
}
