using System;
using Xamarin.Forms;

namespace calculator
{
    public partial class calculatorPage : ContentPage
    {
        string input = "";
        string value1 = "";
        string value2 = "";
        char operation;
        double answer = 0.0;
        public calculatorPage()
        {
            InitializeComponent();
        }
        private void Seven_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "7";
            this.Result.Text += input;
        }
        private void Eight_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "8";
            this.Result.Text += input;
        }
        private void Nine_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "9";
            this.Result.Text += input;

        }
        private void Four_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "4";
            this.Result.Text += input;

        }
        private void Five_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "5";
            this.Result.Text += input;
        }
        private void Six_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "6";
            this.Result.Text += input;
        }
        private void One_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "1";
            this.Result.Text += input;
        }
        private void Two_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "2";
            this.Result.Text += input;
        }
        private void Three_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "3";
            this.Result.Text += input;
        }
        private void Zero_Selected(object sender, EventArgs e)
        {
            this.Result.Text = "";
            input += "0";
            this.Result.Text += input;
        }
        private void Clear_Clicked(object sender, EventArgs e)
        {
            this.Result.Text = "";
            this.input = string.Empty;
            this.value1 = string.Empty;
            this.value2 = string.Empty;
        }
        private void Equal_Clicked(object sender, EventArgs e)
        {
            value2 = input;
            double num1, num2;
            double.TryParse(value1, out num1);
            double.TryParse(value2, out num2);

            if(operation == '+')
            {
                answer = num1 + num2;
                Result.Text = answer.ToString();
            }
            else if(operation == '-')
            {
                answer = num1 - num2;
                Result.Text = answer.ToString();
            }
            else if(operation == 'X')
            {
                answer = num1 * num2;
                Result.Text = answer.ToString();
            }
            else if (operation == '/')
            {
                if (num2 != 0)
                {
                    answer = num1 / num2;
                    Result.Text = answer.ToString();
                }
                    else{
                        Result.Text = "Error";
                    }
            }
        }
        private void Divide_Clicked(object sender, EventArgs e)
        {
            value1 = input;
            operation = '/';
            input = "";
        }
        private void Multiply_Clicked(object sender, EventArgs e)
        {
            value1 = input;
            operation = 'X';
            input = "";
        }
        private void Minus_Clicked(object sender, EventArgs e)
        {
            value1 = input;
            operation = '-';
            input = "";
        }
        private void Plus_Clicked(object sender, EventArgs e)
        {
            value1 = input;
            operation = '+';
            input = "";
        }
    }
}

